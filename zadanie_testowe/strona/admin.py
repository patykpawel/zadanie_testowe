from django.contrib import admin
from .models import Word,AuthorWord
# Register your models here.
admin.site.register(Word)
admin.site.register(AuthorWord)
