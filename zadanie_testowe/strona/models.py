from django.db import models
from django.db.models import F
from .funkcje import autor_tekst
# Create your models here.
class Word(models.Model):
    wyraz=models.CharField(max_length=40)
    ilosc=models.IntegerField(default=1)


class AuthorWord(models.Model):
    wyraz=models.CharField(max_length=40)
    ilosc=models.IntegerField(default=1)
    autor=models.CharField(max_length=30)


for aut,wor in autor_tekst.items():

    for w in wor:
        if Word.objects.filter(wyraz=w).exists():

            Word.objects.filter(wyraz=w).update(ilosc=F('ilosc') + 1)
        else:
            Word.objects.create(wyraz=w)

        if AuthorWord.objects.filter(autor=aut, slowo=w).exists():

            AuthorWord.objects.filter(autor=aut, slowo=w).update(ilosc=F('ilosc') + 1)
        else:
            AuthorWord.objects.create(slowo=w, autor=aut)
