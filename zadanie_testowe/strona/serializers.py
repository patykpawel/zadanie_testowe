from rest_framework import serializers
from .models import Word,AuthorWord

class AuthorWordSerializers(serializers.ModelSerializer):
    class Meta:
        model=AuthorWord
        fields =('wyraz','ilosc')

class WordSerializers(serializers.ModelSerializer):
    class Meta:
        model=Word
        fields =('wyraz','ilosc')