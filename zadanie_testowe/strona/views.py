from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Word,AuthorWord
from .serializers import WordSerializers,AuthorWordSerializers
# Create your views here.

class WordList(APIView):
    def get(self,request):
        wyraz=Word.objects.order_by('-ilosc')[0:10]
        serializer=WordSerializers(wyraz, many=True)
        return Response(serializer.data)

    def post(self):
        pass

class AutorWordList(APIView):
    def get(self,request,autor):
        wyraz = AuthorWord.objects.filter(autor=autor).order_by('-ilosc')[0:10]

        serializer=AuthorWordSerializers(wyraz, many=True)
        return Response(serializer.data)

    def post(self):
        pass