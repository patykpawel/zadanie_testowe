
import requests
from bs4 import BeautifulSoup
from html.parser import HTMLParser


def soup_all_pages(site_url="https://teonite.com/blog/page/1"):
    """
    funkcja pobierajaca wszystkie strony bloga
    :return:zupa z kodem html wszystkich stron
    """
    r = requests.get(site_url)
    try:
        soup = BeautifulSoup(r.text, 'html.parser')

        #wyluskanie tekstu z ilosc stron
        results = soup.find("span", attrs={'class': "page-number"}).text
    except HTMLParseError:
        pass
    results = results.split()

    #ilosc stron na bloga
    num_page = results[-1]

    #dodoanie do zupy wszystkich stron

    for num in range(2, int(num_page) + 1):
        try:
            r = requests.get("https://teonite.com/blog/page/" + str(num))
        except requests.exceptions.RequestException:
            pass
        try:
            soup.append(BeautifulSoup(r.text, 'html.parser'))
        except HTMLParseError:
            pass
    return soup




def get_titles(record):
    """
    funkjca wyłuskająca wysztkie tytuł z zupy
    :param record: zupa z kodem html pomiedzy tagami post-title
    :return:lista z tytułami artykułow
    """
    titles = []

    for title in record:
        titles.append(title.text)
    return titles

def get_urls(record):
    """
    funkcja wyłuskujaca adresy url z zupy

    :param record: zupa z kodem html pomiedzy tagami post-title
    :return: lista z adresami url do kazdego z artykułow
    """
    urls=[]
    for element in record:
        url="https://teonite.com"+str(element.find('a')['href'])
        urls.append(url)
    return urls

def titles_urls_of_articles(soup):
    """
    funkcja zwracajaca liste z tytułami artykulow oraz  linkami do nich

    :param soup: obiekt zwracany z funkcji soup_all_pages
    :return: listy z adresami url i tytułami do kazdego z artykułow
    """
    all_titles_authors=soup.find_all("h2",attrs={'class':'post-title'})
    titles = []
    urls = []
    titles=get_titles(all_titles_authors)
    urls=get_urls(all_titles_authors)

    return titles,urls



def get_authors(urls):
    """
    funkcja wyłuskujca autorów artykułów
    :param urls: lista z adresami url do kazdego artykułu
    :return: lista z autorami artykułów
    """
    authors=[]

    for url in urls:
        try:
            r = requests.get(url)
        except requests.exceptions.RequestException:
            pass
        try:
            soup = BeautifulSoup(r.text, 'html.parser')
            author=soup.find("span", attrs={'class':"author-content"})
        except HTMLParseError:
            pass
        author=str(author.contents[1])
        author=author.replace("<h4>","")
        author=author.replace("</h4>","")
        author=author.replace(" ","")

        authors.append(author.lower())
    return authors




def take_text(urls):
    """
    funkcja pobierajaca tekst artykułu oraz dzielaca go na słowa
    :param urls: lista z adresami url do kazdego artykułu
    :return: lista z wszytkim słowami w artykule
    """

    tekst=""
    wordlist=[]

    for url in urls:
        try:
            soup=BeautifulSoup(requests.get(url).text, 'html.parser')
        except requests.exceptions.RequestException:
            pass
        results=soup.find("section",attrs={'class':"post-content"}).text
        tekst+=results.lower()

    znaki=["\n",".","-","?","!",":",";","\xa0","(",")",'"','”',",","%","“","/"]

    for znak in znaki:
        tekst=tekst.replace(znak," ")

    wordlist=tekst.split(" ")


    while "" in wordlist:
        wordlist.remove("")

    return wordlist


soup=soup_all_pages()

tytul,urls=titles_urls_of_articles(soup)

authors=get_authors(urls)

autorzy_linki={}

#przypisanie autorm linkow z artykułami
for aut,url in zip(authors,urls):
    if aut in autorzy_linki.keys():
        autorzy_linki[aut].append(url)
    else:
        autorzy_linki[aut] = []
        autorzy_linki[aut].append(url)



autor_tekst={}

#przypisanie autorom listy z slowami z ich artykułów
for aut,urls in autorzy_linki.items():

    autor_tekst[aut]=take_text(urls)
